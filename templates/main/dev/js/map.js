var maps = document.querySelectorAll(".map-block__map div");

Array.prototype.forEach.call(maps, function (child) {
	var coords = child.getAttribute("data-coords").split(',');

	function init() {
		var map = new ymaps.Map(child, {
			center: [parseFloat(coords[0]), parseFloat(coords[1])],
			zoom: 12,
			controls: ['zoomControl'],
			behaviors: ['drag']
		});

		var placemark = new ymaps.Placemark([parseFloat(coords[0]), parseFloat(coords[1])], {},
			{
				iconLayout: 'default#image',
				iconImageHref: 'images/map-pin.png',
				iconImageSize: [30, 40],
				iconImageOffset: [-15, -40],
			}
		);

		map.geoObjects.add(placemark);
	}

	ymaps.ready(init);
});

var mapButton = $(".js-map-button");
var overlay = document.querySelector(".js-overlay");

mapButton.on("click", function () {
	for (let i = 0; i < mapButton.length; i++) {
		mapButton[i].parentNode.querySelector(".js-map-block").classList.remove("active");
	}
	overlay.classList.toggle("flag");
	this.parentNode.querySelector(".js-map-block").classList.toggle("active");
})

overlay.addEventListener("click", function () {
	for (let i = 0; i < mapButton.length; i++) {
		mapButton[i].parentNode.querySelector(".js-map-block").classList.remove("active");
	}
})


