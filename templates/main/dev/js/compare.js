$(document).ready(function () {
	(function () {
		var compareSwiper = document.querySelector(".js-compare-swiper");
		var compareSwiperBottom = document.querySelector(".js-compare-swiper-bottom");
		var swiperSlide = document.querySelectorAll(".compare__swiper .swiper-slide");

		var swiper = new Swiper(compareSwiper, {
			slidesPerView: 3,
			scrollbar: {
				el: '.swiper-scrollbar',
				draggable: true
			},
			breakpoints: {
				1160: {
					slidesPerView: 1,
					pagination: {
						el: '.swiper-pagination',
						type: 'bullets',
						clickable: true
					},
					scrollbar: {
						el: false
					}
				},
				767: {
					slidesPerView: 2
				}
			},
			on: {
				touchMove: function () {
					swiperBottom.setTranslate(this.translate);
				},
				slideChangeTransitionStart: function () {
					swiperBottom.setTranslate(this.translate);
				}
			}
		});

		var swiperBottom = new Swiper(compareSwiperBottom, {
			slidesPerView: 3,
			scrollbar: {
				el: '.swiper-scrollbar',
				draggable: true
			},
			on: {
				touchMove: function () {
					swiper.setTranslate(this.translate);
				},
				slideChangeTransitionStart: function () {
					swiper.setTranslate(this.translate);
				}
			}
		});

		Array.prototype.forEach.call(swiperSlide, function (child) {
			var compareClose = child.querySelector(".js-compare__close");
			compareClose.addEventListener("click", function () {
				var index = compareClose.parentNode.parentNode.getAttribute("data-slide");
				compareClose.parentNode.parentNode.parentNode.removeChild(compareClose.parentNode.parentNode);
				swiperBottom.update();
				swiper.update();
				swiperBottom.removeSlide(index);
				var bottomSlides = compareSwiperBottom.querySelectorAll(".swiper-slide");

				for (let i = 0; i < bottomSlides.length; i++) {
					var indexBottom = bottomSlides[i].getAttribute("data-slide");
					if (indexBottom == index) {
						bottomSlides[i].parentNode.removeChild(bottomSlides[i]);
					}
				}
			})
		});

		var scroolbar = $(".swiper-scrollbar_top .swiper-scrollbar-drag");

		scroolbar.on("mousemove", function () {
			scroolbar.addClass("active");
			swiperBottom.setTranslate(swiper.translate);
		})

		scroolbar.on("mouseup", function () {
			scroolbar.removeClass("active");

		})

		var scroolbarBottom = $(".swiper-scrollbar_bottom .swiper-scrollbar-drag");

		scroolbarBottom.on("mousemove", function () {
			scroolbarBottom.addClass("active");
			swiper.setTranslate(swiperBottom.translate);
		})

		scroolbarBottom.on("mouseup", function () {
			scroolbarBottom.removeClass("active");

		})
	})();

	(function () {
		var mqTablet = window.matchMedia("(max-width: 1160px) and (min-width:768px)");
		var mqMobile = window.matchMedia("(max-width: 768px)");
		var compareItem = document.querySelector(".js-compare-swiper");

		var compareItemAccordion1 = document.querySelector(".js-swiper-accordion-1");
		var compareItemAccordion2 = document.querySelector(".js-swiper-accordion-2");
		var compareItemAccordion3 = document.querySelector(".js-swiper-accordion-3");
		var compareItemAccordion4 = document.querySelector(".js-swiper-accordion-4");
		var compareItemAccordion5 = document.querySelector(".js-swiper-accordion-5");
		var compareItemAccordion6 = document.querySelector(".js-swiper-accordion-6");

		function cloneSwipers(tabletIndex) {
			for (let i = 0; i < tabletIndex; i++) {
				var newCompareItem = compareItem.cloneNode(true);
				compareItem.parentNode.appendChild(newCompareItem);
				newCompareItem.setAttribute("data-swiper", i);
				newCompareItem.classList.add("data-swiper-" + i);
			}
			compareItem.parentNode.removeChild(compareItem);

			for (let i = 0; i < tabletIndex; i++) {
				var newcompareItemAccordion1 = compareItemAccordion1.cloneNode(true);
				compareItemAccordion1.parentNode.appendChild(newcompareItemAccordion1);
				newcompareItemAccordion1.setAttribute("data-swiper", i);
				newcompareItemAccordion1.classList.add("data-swiper-" + i);
			}
			compareItemAccordion1.parentNode.removeChild(compareItemAccordion1);

			for (let i = 0; i < tabletIndex; i++) {
				var newcompareItemAccordion2 = compareItemAccordion2.cloneNode(true);
				compareItemAccordion2.parentNode.appendChild(newcompareItemAccordion2);
				newcompareItemAccordion2.setAttribute("data-swiper", i);
				newcompareItemAccordion2.classList.add("data-swiper-" + i);
			}
			compareItemAccordion2.parentNode.removeChild(compareItemAccordion2);

			for (let i = 0; i < tabletIndex; i++) {
				var newcompareItemAccordion3 = compareItemAccordion3.cloneNode(true);
				compareItemAccordion3.parentNode.appendChild(newcompareItemAccordion3);
				newcompareItemAccordion3.setAttribute("data-swiper", i);
				newcompareItemAccordion3.classList.add("data-swiper-" + i);
			}
			compareItemAccordion3.parentNode.removeChild(compareItemAccordion3);

			for (let i = 0; i < tabletIndex; i++) {
				var newcompareItemAccordion4 = compareItemAccordion4.cloneNode(true);
				compareItemAccordion4.parentNode.appendChild(newcompareItemAccordion4);
				newcompareItemAccordion4.setAttribute("data-swiper", i);
				newcompareItemAccordion4.classList.add("data-swiper-" + i);
			}
			compareItemAccordion4.parentNode.removeChild(compareItemAccordion4);

			for (let i = 0; i < tabletIndex; i++) {
				var newcompareItemAccordion5 = compareItemAccordion5.cloneNode(true);
				compareItemAccordion5.parentNode.appendChild(newcompareItemAccordion5);
				newcompareItemAccordion5.setAttribute("data-swiper", i);
				newcompareItemAccordion5.classList.add("data-swiper-" + i);
			}
			compareItemAccordion5.parentNode.removeChild(compareItemAccordion5);

			for (let i = 0; i < tabletIndex; i++) {
				var newcompareItemAccordion6 = compareItemAccordion6.cloneNode(true);
				compareItemAccordion6.parentNode.appendChild(newcompareItemAccordion6);
				newcompareItemAccordion6.setAttribute("data-swiper", i);
				newcompareItemAccordion6.classList.add("data-swiper-" + i);
			}
			compareItemAccordion6.parentNode.removeChild(compareItemAccordion6);
		}

		function addSwipers(item) {
			var compareTopSwiper = document.querySelector(".js-compare-swiper.data-swiper-" + item);
			var compareAccordionSwiper1 = document.querySelector(".js-swiper-accordion-1.data-swiper-" + item);
			var compareAccordionSwiper2 = document.querySelector(".js-swiper-accordion-2.data-swiper-" + item);
			var compareAccordionSwiper3 = document.querySelector(".js-swiper-accordion-3.data-swiper-" + item);
			var compareAccordionSwiper4 = document.querySelector(".js-swiper-accordion-4.data-swiper-" + item);
			var compareAccordionSwiper5 = document.querySelector(".js-swiper-accordion-5.data-swiper-" + item);
			var compareAccordionSwiper6 = document.querySelector(".js-swiper-accordion-6.data-swiper-" + item);
			var compareTopItems = compareTopSwiper.querySelectorAll(".swiper-slide");
			var compareActiveNumber = compareTopSwiper.querySelector(".js-swiper-indicate-index");
			var compareAllNumber = compareTopSwiper.querySelector(".js-swiper-indicate-all");
			var swiperTop;

			function initSwipers() {
				swiperTop = new Swiper(compareTopSwiper, {
					slidesPerView: 1,
					pagination: {
						el: '.swiper-pagination',
						type: 'bullets',
						clickable: true
					},
					on: {
						slideChange: function () {
							swiperAccordion1.slideTo(this.activeIndex);
							swiperAccordion2.slideTo(this.activeIndex);
							swiperAccordion3.slideTo(this.activeIndex);
							swiperAccordion4.slideTo(this.activeIndex);
							swiperAccordion5.slideTo(this.activeIndex);
							swiperAccordion6.slideTo(this.activeIndex);
							compareActiveNumber.innerHTML = this.activeIndex + 1;
						}
					}
				});

				var swiperAccordion1 = new Swiper(compareAccordionSwiper1, {
					slidesPerView: 1,
					on: {
						slideChange: function () {
							swiperTop.slideTo(this.activeIndex);
							swiperAccordion2.slideTo(this.activeIndex);
							swiperAccordion3.slideTo(this.activeIndex);
							swiperAccordion4.slideTo(this.activeIndex);
							swiperAccordion5.slideTo(this.activeIndex);
							swiperAccordion6.slideTo(this.activeIndex);
						}
					}
				});

				var swiperAccordion2 = new Swiper(compareAccordionSwiper2, {
					slidesPerView: 1,
					on: {
						slideChange: function () {
							swiperTop.slideTo(this.activeIndex);
							swiperAccordion1.slideTo(this.activeIndex);
							swiperAccordion3.slideTo(this.activeIndex);
							swiperAccordion4.slideTo(this.activeIndex);
							swiperAccordion5.slideTo(this.activeIndex);
							swiperAccordion6.slideTo(this.activeIndex);
						}
					}
				});

				var swiperAccordion3 = new Swiper(compareAccordionSwiper3, {
					slidesPerView: 1,
					on: {
						slideChange: function () {
							swiperTop.slideTo(this.activeIndex);
							swiperAccordion1.slideTo(this.activeIndex);
							swiperAccordion2.slideTo(this.activeIndex);
							swiperAccordion4.slideTo(this.activeIndex);
							swiperAccordion5.slideTo(this.activeIndex);
							swiperAccordion6.slideTo(this.activeIndex);
						}
					}
				});

				var swiperAccordion4 = new Swiper(compareAccordionSwiper4, {
					slidesPerView: 1,
					on: {
						slideChange: function () {
							swiperTop.slideTo(this.activeIndex);
							swiperAccordion1.slideTo(this.activeIndex);
							swiperAccordion2.slideTo(this.activeIndex);
							swiperAccordion3.slideTo(this.activeIndex);
							swiperAccordion5.slideTo(this.activeIndex);
							swiperAccordion6.slideTo(this.activeIndex);
						}
					}
				});

				var swiperAccordion5 = new Swiper(compareAccordionSwiper5, {
					slidesPerView: 1,
					on: {
						slideChange: function () {
							swiperTop.slideTo(this.activeIndex);
							swiperAccordion1.slideTo(this.activeIndex);
							swiperAccordion2.slideTo(this.activeIndex);
							swiperAccordion3.slideTo(this.activeIndex);
							swiperAccordion4.slideTo(this.activeIndex);
							swiperAccordion6.slideTo(this.activeIndex);
						}
					}
				});

				var swiperAccordion6 = new Swiper(compareAccordionSwiper6, {
					slidesPerView: 1,
					on: {
						slideChange: function () {
							swiperTop.slideTo(this.activeIndex);
							swiperAccordion1.slideTo(this.activeIndex);
							swiperAccordion2.slideTo(this.activeIndex);
							swiperAccordion3.slideTo(this.activeIndex);
							swiperAccordion4.slideTo(this.activeIndex);
							swiperAccordion5.slideTo(this.activeIndex);
						}
					}
				});
			}

			initSwipers();


			Array.prototype.forEach.call(compareTopItems, function (child) {
				var compareClose = child.querySelector(".js-compare__close");
				compareClose.addEventListener("click", function () {
					var index = compareClose.parentNode.parentNode.getAttribute("data-slide");
					compareClose.parentNode.parentNode.parentNode.removeChild(compareClose.parentNode.parentNode);
					for (let i = 0; i < compareTopSwiper.length; i++) {
						compareTopSwiper[i].update();
						swiperTop.pagination.update();
					}

					function updateAfterClose() {
						compareAllNumber.innerHTML -= 1;
						swiperTop.update();
						swiperTop.pagination.update();
						swiperTop.slideNext();
						var bottomSlidesAccordion1 = compareAccordionSwiper1.querySelectorAll(".swiper-slide");

						for (let i = 0; i < bottomSlidesAccordion1.length; i++) {
							let indexBottom = bottomSlidesAccordion1[i].getAttribute("data-slide");
							if (indexBottom == index) {
								bottomSlidesAccordion1[i].parentNode.removeChild(bottomSlidesAccordion1[i]);
							}
						}

						var bottomSlidesAccordion2 = compareAccordionSwiper2.querySelectorAll(".swiper-slide");

						for (let i = 0; i < bottomSlidesAccordion2.length; i++) {
							let indexBottom = bottomSlidesAccordion2[i].getAttribute("data-slide");
							if (indexBottom == index) {
								bottomSlidesAccordion2[i].parentNode.removeChild(bottomSlidesAccordion2[i]);
							}
						}

						var bottomSlidesAccordion3 = compareAccordionSwiper3.querySelectorAll(".swiper-slide");

						for (let i = 0; i < bottomSlidesAccordion3.length; i++) {
							let indexBottom = bottomSlidesAccordion3[i].getAttribute("data-slide");
							if (indexBottom == index) {
								bottomSlidesAccordion3[i].parentNode.removeChild(bottomSlidesAccordion3[i]);
							}
						}

						var bottomSlidesAccordion4 = compareAccordionSwiper4.querySelectorAll(".swiper-slide");

						for (let i = 0; i < bottomSlidesAccordion4.length; i++) {
							let indexBottom = bottomSlidesAccordion4[i].getAttribute("data-slide");
							if (indexBottom == index) {
								bottomSlidesAccordion4[i].parentNode.removeChild(bottomSlidesAccordion4[i]);
							}
						}

						var bottomSlidesAccordion5 = compareAccordionSwiper5.querySelectorAll(".swiper-slide");

						for (let i = 0; i < bottomSlidesAccordion5.length; i++) {
							let indexBottom = bottomSlidesAccordion5[i].getAttribute("data-slide");
							if (indexBottom == index) {
								bottomSlidesAccordion5[i].parentNode.removeChild(bottomSlidesAccordion5[i]);
							}
						}

						var bottomSlidesAccordion6 = compareAccordionSwiper6.querySelectorAll(".swiper-slide");

						for (let i = 0; i < bottomSlidesAccordion6.length; i++) {
							let indexBottom = bottomSlidesAccordion6[i].getAttribute("data-slide");
							if (indexBottom == index) {
								bottomSlidesAccordion6[i].parentNode.removeChild(bottomSlidesAccordion6[i]);
							}
						}

						var emptyContainer = compareTopSwiper.querySelector(".swiper-wrapper");
						var parentEmptyContainer = emptyContainer.parentNode;
						var currentSwiper = parentEmptyContainer.getAttribute("data-swiper");

						if (!emptyContainer.firstElementChild) {
							var emptySwiperAccordion1 = document.querySelector(".js-swiper-accordion-1.data-swiper-" + currentSwiper);
							var emptySwiperAccordion2 = document.querySelector(".js-swiper-accordion-2.data-swiper-" + currentSwiper);
							var emptySwiperAccordion3 = document.querySelector(".js-swiper-accordion-3.data-swiper-" + currentSwiper);
							var emptySwiperAccordion4 = document.querySelector(".js-swiper-accordion-4.data-swiper-" + currentSwiper);
							var emptySwiperAccordion5 = document.querySelector(".js-swiper-accordion-5.data-swiper-" + currentSwiper);
							var emptySwiperAccordion6 = document.querySelector(".js-swiper-accordion-6.data-swiper-" + currentSwiper);
							parentEmptyContainer.parentNode.removeChild(parentEmptyContainer);
							emptySwiperAccordion1.parentNode.removeChild(emptySwiperAccordion1);
							emptySwiperAccordion2.parentNode.removeChild(emptySwiperAccordion2);
							emptySwiperAccordion3.parentNode.removeChild(emptySwiperAccordion3);
							emptySwiperAccordion4.parentNode.removeChild(emptySwiperAccordion4);
							emptySwiperAccordion5.parentNode.removeChild(emptySwiperAccordion5);
							emptySwiperAccordion6.parentNode.removeChild(emptySwiperAccordion6);
						}
					}

					updateAfterClose();

				})
			});
		}

		if (mqTablet.matches) {
			cloneSwipers(4);

			for (let i = 0; i < 4; i++) {
				addSwipers(i);
			}
		}

		if (mqMobile.matches) {
			cloneSwipers(2);

			for (let i = 0; i < 2; i++) {
				addSwipers(i);
			}
		}

	})();

	(function () {
		var leftBlocks = $(".compare__left .compare__block");
		var rightSwipers = $(".compare__right .swiper-slide");

		for (var c = 0; c < leftBlocks.length; c++) {
			var dataAttr = leftBlocks[c].getAttribute("data-block");

			for (let i = 0; i < rightSwipers.length; i++) {

				var compareBlocks = rightSwipers[i].querySelectorAll(".compare__block");

				for (let k = 0; k < compareBlocks.length; k++) {
					var dataBlock = compareBlocks[k].getAttribute("data-block");

					if (dataAttr == dataBlock) {
						var leftValues = leftBlocks[c].querySelectorAll(".compare__value");
						var rightValues = compareBlocks[c].querySelectorAll(".compare__value");

						for (var a = 0; a < leftValues.length; a++) {

							for (var j = 0; j < rightValues.length; j++) {
								if (rightValues[a].clientHeight > leftValues[a].clientHeight) {
									leftValues[a].style.height = rightValues[a].clientHeight + "px";
									for (let b = 0; b < rightSwipers.length; b++) {
										var otherBlocks = rightSwipers[b].querySelectorAll(".compare__block");

										for (let d = 0; d < otherBlocks.length; d++) {
											var dataOtherBlock = otherBlocks[d].getAttribute("data-block");

											if (dataAttr == dataOtherBlock) {
												var otherValues = otherBlocks[d].querySelectorAll(".compare__value");
												for (let h = 0; h < otherValues.length; h++) {
													otherValues[a].style.height = rightValues[a].clientHeight + "px";
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	})();

	(function () {
		var compareAll = document.querySelector(".js-compare-all");
		var compareDiff = document.querySelector(".js-compare-diff");
		var compareBottom = document.querySelector(".js-compare__bottom");
		var countBlocks = document.querySelector(".js-compare__left").children.length;

		compareDiff.addEventListener("click", function () {
			compareBottom.classList.add("delete");
			this.classList.add("active");
			compareAll.classList.remove("active");
			compareValues(true);
			$.each($(".js-compare__block"), function () {
				if (!$(this).children().hasClass("diff")) {
					$(this).addClass("hide");
				}
			});

		});

		compareAll.addEventListener("click", function () {
			this.classList.add("active");
			compareDiff.classList.remove("active");
			compareBottom.classList.remove("delete");
			compareValues(false);
			$.each($(".js-compare__block"), function () {
				$(this).removeClass("hide");
			})
		});

		function compareValues(deleting) {
			for (let i = 0; i < countBlocks; i++) {
				var compareBlocks = document.querySelectorAll(".compare__right [data-block='" + i + "']");
				var compareLeft = document.querySelector(".compare__left [data-block='" + i + "']");
				Array.prototype.forEach.call(compareBlocks, function (child) {
					for (let k = 0; k < child.children.length; k++) {
						var index = k;
						var compareValue = child.children[k];
						Array.prototype.forEach.call(compareBlocks, function (item) {
							for (let j = 0; j < item.children.length; j++) {
								var compareOtherValue = item.children[index];
								if (compareValue.innerHTML !== compareOtherValue.innerHTML) {
									if (deleting) {
										compareValue.classList.add('diff');
										compareLeft.children[index + 1].classList.add('diff');
									}
									else {
										compareValue.classList.remove('diff');
										compareLeft.children[index + 1].classList.remove('diff');
									}
								}
							}
						});
					}
				});

				if (deleting) {
					if (compareLeft) {
						if (!compareLeft.querySelectorAll(".diff").length) {
							compareLeft.style.display = "none";
						}
					}
				}

				else {
					if (compareLeft) {
						compareLeft.style.display = "block";
					}
				}


			}
		}

		var mq = window.matchMedia("(max-width: 1160px)");
		if (mq.matches) {
			var accordionCompare = document.querySelector(".js-accordion-compare");
			var accordionBlock = $(".accordion__block");

			compareDiff.addEventListener("click", function () {
				accordionCompare.classList.add("delete");
				diffTablet();
				$.each((accordionBlock), function () {
					var compareBlock = $(this).find(".compare__block");
					if (!compareBlock.children().hasClass("diff")) {
						$(this).addClass("hide");
					}
				});
			});

			compareAll.addEventListener("click", function () {
				accordionCompare.classList.remove("delete");
				diffTablet();
				$.each((accordionBlock), function () {
					$(this).removeClass("hide");
				})
			});
		}

		function diffTablet() {
			var accordion = document.querySelector(".js-accordion-compare");
			var accordionBlock = document.querySelectorAll(".accordion__block .accordion__panel");

			for (let y = 0; y < accordionBlock.length; y++) {
				var swiperBlock = accordionBlock[y].querySelectorAll(".swiper-slide");

				for (let i = 0; i < swiperBlock.length; i++) {
					var chidrenCompare = swiperBlock[i].querySelectorAll(".compare__unit");

					for (let j = 0; j < chidrenCompare.length; j++) {
						var index = j;

						for (let k = 0; k < swiperBlock.length; k++) {
							var chidrenOtherCompare = swiperBlock[k].querySelectorAll(".compare__unit");

							for (let item = 0; item < chidrenOtherCompare.length; item++) {

								if (chidrenCompare[j].innerHTML !== chidrenOtherCompare[index].innerHTML) {
									chidrenCompare[j].classList.add("diff");
								}
							}

						}
					}
				}
			}
		}

	})();

	(function () {
		var mq = window.matchMedia("(min-width: 1160px)");
		if (mq.matches) {
			$(".wrapper").on("scroll", function () {
				if (($(".wrapper").scrollTop() > 270) && ($(".js-recent").offset().top > 270)) {
					$('.js-compare-fixed').addClass('fixed');
					$(".js-compare__bottom").addClass("padding");
				}
				else {
					$('.js-compare-fixed').removeClass('fixed');
					$(".js-compare__bottom").removeClass("padding");
				}
			});
		}
	})();

});
