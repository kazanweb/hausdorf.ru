$(document).ready(function () {

	window.globalPopup = new Popup();

	var tooltip = new Tooltip('tooltip');

	addVoidForLinks($("a"));

	var overlay = document.querySelector(".js-overlay");
	var mq = window.matchMedia("(max-width: 767px)");

	var phoneMask = document.querySelectorAll(".js-phone");
	Array.prototype.forEach.call(phoneMask, function (child) {
		phoneMask = new IMask(child, {
			mask: '000 000-00-00'
		});

	});

	$(".js-scroll").click(function (e) {
		var elementClick = $(this).attr("href");
		if ($(elementClick).length) {
			var destination = $(elementClick).offset().top;
			$('html,body').stop().animate({scrollTop: destination}, 1000);
		}

	});

	$(document).on("click", "[data-ajax]", function (e) {
		e.stopPropagation();
		e.preventDefault();
		$.get(this.getAttribute("data-url"), function (response) {
			globalPopup.html(response).show();
		});
	});

	(function () {
		var mq = window.matchMedia("(max-width: 500px)");
		if (mq.matches) {
			var breadcrumbsList = document.querySelector(".breadcrumbs__list");
			if (breadcrumbsList.children.length >= 4) {
				var breadcrumbsItem = document.querySelectorAll(".breadcrumbs__item");
				for (let i = 0; i < breadcrumbsItem.length; i++) {
					if (breadcrumbsItem[i].offsetWidth > 190) {
						breadcrumbsItem[i].classList.add("cut");
					}
				}
			}
		}
	})();

	(function () {
		$('.js-callback').click(function () {
			$.post("ajax/callback.html", function (response) {
				globalPopup.html(response, function () {
					validate(".js-form");

				}).show();
			});
			return false;
		})

		$('.js-header-manager').click(function () {
			$.post("ajax/departure.html", function (response) {
				globalPopup.html(response, function () {
					validate(".js-form");

				}).show();
			});
			return false;
		})

		$('.js-abuse').click(function () {
			$.post("ajax/abuse.html", function (response) {
				globalPopup.html(response, function () {
					validate(".js-form");

				}).show();
			});
			return false;
		})

		$('.js-reviews').click(function () {
			$.post("ajax/review.html", function (response) {
				globalPopup.html(response, function () {
					validate(".js-form");
					var grade = document.querySelector(".js-review__number");

					new Ratings({
						element: document.querySelector('.js-ratings__section'),	// передаем элемент
						countRate: 5,							//  кол-во оценок, необязательный параметр, по умолчанию стоит 5
						clickFn: function (index) {
							grade.innerHTML = index;
						}
					});
				}).show();
			});
			return false;
		})

		$('.js-basket-add').click(function () {
			$.post("ajax/basket-add.html", function (response) {
				globalPopup.html(response, function () {
					var mq = window.matchMedia("(min-width: 768px)");
					if (mq.matches) {
						var swiper = new Swiper('.js-relative-basket-swiper', {
							slidesPerView: 2.5,
							spaceBetween: 10,
							scrollbar: {
								el: '.swiper-scrollbar'
							},
							breakpoints: {
								768: {
									slidesPerView: 1,
									spaceBetween: 20
								}
							}
						});
					}

				}).show();
			});
			return false;
		})
	})();

	(function () {
		if (!$(".js-gallery").length) {
			return false
		}
		gallery(".js-gallery");
	})();

	var sliderSwiper = new Swiper('.js-swiper-slider', {
		loop: true,
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true
		},
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		preloadImages: false,
		lazy: true
	})

	var popularSwiper = new Swiper('.js-swiper-popular', {
		navigation: {
			nextEl: '.swiper-button-next',
			prevEl: '.swiper-button-prev',
		},
		slidesPerView: 1,
		preloadImages: false,
		lazy: true,
		on: {
			slideChange: function () {
				var activeIndex = popularSwiper.activeIndex;
				var swiperTopSlide = $(".js-swiper-top-popular .swiper-slide[data-slide=" + activeIndex + "]");

				$.each($(".js-swiper-top-popular .swiper-slide"), function () {
					$(this).removeClass("active");
				});

				swiperTopSlide.addClass("active");
				popularTopSwiper.slideTo(activeIndex);
			},
			touchMove: function () {
				var activeIndex = popularSwiper.activeIndex;
				var swiperTopSlide = $(".js-swiper-top-popular .swiper-slide[data-slide=" + activeIndex + "]");

				$.each($(".js-swiper-top-popular .swiper-slide"), function () {
					$(this).removeClass("active");
				});

				swiperTopSlide.addClass("active");
				popularTopSwiper.slideTo(activeIndex);
			}
		}
	})

	var popularTopSwiper = new Swiper('.js-swiper-top-popular', {
		slidesPerView: 6,
		spaceBetween: 30,
		preloadImages: false,
		lazy: true,
		on: {
			slideChange: function () {
				var activeIndex = popularTopSwiper.activeIndex;
				popularSwiper.slideTo(activeIndex);
			}
		},
		breakpoints: {
			1000: {
				centeredSlides: true,
				slidesPerView: 3.5
			},
			767: {
				centeredSlides: true,
				slidesPerView: 2.5
			}
		}
	})


	var similarSwiper = new Swiper('.js-similar-swiper', {
		slidesPerView: 4.5,
		spaceBetween: 10,
		scrollbar: {
			el: '.swiper-scrollbar',
			draggable: true
		}
	});


	var collectionSwiper = new Swiper('.js-swiper-collection', {
		slidesPerView: 3,
		spaceBetween: 10,
		scrollbar: {
			el: '.swiper-scrollbar',
			draggable: true
		},
		breakpoints: {
			1000: {
				slidesPerView: 2,
				spaceBetween: 20
			},
			768: {
				slidesPerView: 2,
				spaceBetween: 10
			}
		}
	});

	var cardSwiper = new Swiper('.js-swiper-card', {
		slidesPerView: 1,
		loop: true,
		pagination: {
			el: '.swiper-pagination',
			type: 'bullets',
			clickable: true
		}
	});

	(function () {
		var swiperTopPoplular = $(".js-swiper-top-popular .swiper-slide");
		$.each(swiperTopPoplular, function () {
			var activeIndex = $(this).attr("data-slide");

			$(this).on("click", function () {
				$.each(swiperTopPoplular, function () {
					$(this).removeClass("active");
				});

				$(this).addClass("active");
				popularSwiper.slideTo(activeIndex);
			})
		});
	})();


	var historySwiper = new Swiper('.js-swiper-history', {
		slidesPerView: 1,
		on: {
			slideChange: function () {
				var activeIndex = historySwiper.activeIndex;
				var swiperTopSlide = $(".js-swiper-top-history .swiper-slide[data-slide=" + activeIndex + "]");

				$.each($(".js-swiper-top-history .swiper-slide"), function () {
					$(this).removeClass("active");
				});

				swiperTopSlide.addClass("active");
				historyTopSwiper.slideTo(activeIndex);
			},
			touchMove: function () {
				var activeIndex = popularSwiper.activeIndex;
				var swiperTopSlide = $(".js-swiper-top-history .swiper-slide[data-slide=" + activeIndex + "]");

				$.each($(".js-swiper-top-history .swiper-slide"), function () {
					$(this).removeClass("active");
				});

				swiperTopSlide.addClass("active");
				historyTopSwiper.slideTo(activeIndex);
			}
		}
	});

	var historyTopSwiper = new Swiper('.js-swiper-top-history', {
		slidesPerView: 7,
		on: {
			slideChange: function () {
				var activeIndex = historyTopSwiper.activeIndex;
				historySwiper.slideTo(activeIndex);
			}
		},
		breakpoints: {
			1000: {
				centeredSlides: true,
				slidesPerView: 5
			},
			767: {
				centeredSlides: true,
				slidesPerView: 2.5
			}
		}
	});

	(function () {
		var swiperTopHistory = $(".js-swiper-top-history .swiper-slide");
		$.each(swiperTopHistory, function () {
			var activeIndex = $(this).attr("data-slide");

			$(this).on("click", function () {
				$.each(swiperTopHistory, function () {
					$(this).removeClass("active");
				});

				$(this).addClass("active");
				historySwiper.slideTo(activeIndex);
			})
		});
	})();

	(function () {
		var mq = window.matchMedia("(min-width: 1000px)");
		if (mq.matches) {
			var swiper = new Swiper('.js-recent-swiper', {
				slidesPerView: 4,
				spaceBetween: 10,
				lazy: true,
				scrollbar: {
					el: '.swiper-scrollbar',
					draggable: true
				},
			});
		}

	})();

	(function () {
		var mq = window.matchMedia("(min-width: 1140px)");
		if (mq.matches) {
			var swiper = new Swiper('.js-relative-swiper', {
				slidesPerView: 4,
				spaceBetween: 25,
				scrollbar: {
					el: '.swiper-scrollbar',
					draggable: true
				},
			});
		}

	})();

	(function () {

		var mq = window.matchMedia("(min-width: 1000px)");
		if (mq.matches) {

			var swiper = new Swiper('.js-articles-swiper', {
				slidesPerView: 4,
				spaceBetween: 20,
				scrollbar: {
					el: '.swiper-scrollbar',
					draggable: true
				}
			});
		}

	})();
	(function () {
		var overlay = $(".js-overlay");
		var address = $(".js-select__address");
		$('.js-select').on('click', '.select__placeholder', function () {
			var parent = $(this).closest('.select');
			if (!parent.hasClass('is-open')) {
				parent.addClass('is-open');
				if (!$(this).parent().hasClass("js-select-city-header")) {
					overlay.addClass("flag");
				}
				$('.select.is-open').not(parent).removeClass('is-open');

			} else {
				parent.removeClass('is-open');
				overlay.removeClass("flag");
			}
		}).on('click', '.js-select__item', function () {
			var parent = $(this).closest('.select');
			$(this).siblings().removeClass("active");
			$(this).addClass("active");
			parent.removeClass('is-open').find('.select__placeholder span').text($(this).text());
			overlay.removeClass("flag");
			address.html($(this).attr("data-text"));
			if ($(this).hasClass("js-select-city")) {
				address.html(parent.find('.select__placeholder span').text($(this).text()));
			}
			parent.find('input[type=hidden]').attr('value', $(this).attr('data-value'));
		});

		overlay.on("click", function () {
			$('.js-select').removeClass('is-open');
			overlay.removeClass("flag");
		})
	})();
	(function () {
		var catalogButton = document.querySelector(".js-catalog-button");
		var catalog = document.querySelector(".js-catalog");
		var brandsButton = document.querySelector(".js-brands-button");
		var brands = document.querySelector(".js-brands");
		var headerMobile = document.querySelector(".js-header-mobile");
		var headerPhone = document.querySelectorAll(".js-header-phone");

		catalogButton.addEventListener("click", function (e) {
			catalog.classList.toggle("active");
			catalogButton.classList.toggle("active");
			overlay.classList.toggle("active");
			brands.classList.remove("active");
			brandsButton.classList.remove("active");

			if (mq.matches) {
				headerMobile.classList.add("active");
				headerMobile.parentNode.parentNode.classList.add("active");
				brandsButton.classList.add("mobile");
				catalogButton.classList.add("active");
				catalog.classList.add("active");
				overlay.classList.add("active");

				headerPhone.forEach(function (item, i, arr) {
					item.classList.add("active");
				});

			}
		})

		brandsButton.addEventListener("click", function (e) {
			brands.classList.toggle("active");
			brandsButton.classList.toggle("active");
			overlay.classList.toggle("active");
			catalog.classList.remove("active");
			catalogButton.classList.remove("active");

			if (mq.matches) {
				brands.classList.add("active");
				catalogButton.classList.add("mobile");
				overlay.classList.add("active");

			}
		})

		overlay.addEventListener("click", function () {
			brands.classList.remove("active");
			overlay.classList.remove("active");
			brandsButton.classList.remove("active");
			catalog.classList.remove("active");
			catalogButton.classList.remove("active");
		})

	})();

	(function () {
		if (mq.matches) {

			document.addEventListener("click", function (e) {
				var catalog = document.querySelector(".js-catalog");
				var catalogButton = document.querySelector(".js-catalog-button");
				var brandsButton = document.querySelector(".js-brands-button");
				var brands = document.querySelector(".js-brands");
				var headerMobile = document.querySelector(".js-header-mobile");
				var headerPhone = document.querySelectorAll(".js-header-phone");

				var mousePos = e.clientX;
				if (mousePos > 291) {
					removeActiveClass();
				}

				function removeActiveClass() {
					overlay.classList.remove("active");
					catalog.classList.remove("active");
					brandsButton.classList.remove("active");
					brandsButton.classList.remove("mobile");
					brands.classList.remove("active");
					headerMobile.classList.remove("active");
					headerMobile.parentNode.parentNode.classList.remove("active");
					catalogButton.classList.remove("active");
					headerPhone.forEach(function (item, i, arr) {
						item.classList.remove("active");
					});
				}

				function swipeMenu() {
					// Get a reference to an element
					var square = document.querySelector('.js-overlay');

// Create a manager to manager the element
					var manager = new Hammer.Manager(square);

// Create a recognizer
					var Swipe = new Hammer.Swipe();

// Add the recognizer to the manager
					manager.add(Swipe);

// Declare global variables to swiped correct distance
					var deltaX = 0;
					var deltaY = 0;

// Subscribe to a desired event
					manager.on('swipe', function (e) {
						deltaX = deltaX + e.deltaX;
						var direction = e.offsetDirection;
						var translate3d = 'translate3d(' + deltaX + 'px, 0, 0)';

						if (direction === 2) {
							removeActiveClass();
						}
					});
				};
				swipeMenu();
			})
		}
	})();

	(function () {
		if (mq.matches) {
			var catalogMore = document.querySelectorAll(".js-catalog__more");
			var catalogList = document.querySelectorAll(".js-catalog-list");

			var catalogList = Array.prototype.slice.call(catalogList);

			catalogList.forEach(function (item, i, arr) {
				var newArr = [];
				var arr;
				for (let j = 0; j < item.children.length; j++) {
					newArr.push(item.children[j]);
					if (j > 4) {
						item.children[j].style.display = "none";
					}
				}
				arr = newArr.slice(5);
			});

			Array.prototype.forEach.call(catalogMore, function (child) {
				child.addEventListener("click", function () {
					render(child);
				})
			});
		}

		function render(element) {
			var cat = element.previousElementSibling;

			if (element.classList.contains("active")) {
				element.classList.remove("active");
				element.innerHTML = "Показать еще";

				for (var item = 0; item < cat.children.length; item++) {
					if (item > 4) {
						cat.children[item].style.display = "none";
					}
				}
			}

			else {
				element.classList.add("active");
				element.innerHTML = "Свернуть";

				for (let item = 0; item < cat.children.length; item++) {
					cat.children[item].style.display = "block";
				}
			}
		}
	})();

	(function () {

		var catalogMore = document.querySelectorAll(".js-search-menu__more");
		var catalogList = document.querySelectorAll(".js-search-menu__list");
		var catalogList = Array.prototype.slice.call(catalogList);

		catalogList.forEach(function (item, i, arr) {
			var newArr = [];
			var arr;
			for (let j = 0; j < item.children.length; j++) {
				newArr.push(item.children[j]);
				if (j > 4) {
					item.children[j].style.display = "none";
				}
			}
			arr = newArr.slice(5);
		});

		Array.prototype.forEach.call(catalogMore, function (child) {
			child.addEventListener("click", function () {
				render(child);
			})
		});

		function render(element) {
			var cat = element.previousElementSibling;

			if (element.classList.contains("active")) {
				element.classList.remove("active");
				element.innerHTML = "Показать еще";

				for (var item = 0; item < cat.children.length; item++) {
					if (item > 4) {
						cat.children[item].style.display = "none";
					}
				}
			}

			else {
				element.classList.add("active");
				element.innerHTML = "Свернуть";

				for (let item = 0; item < cat.children.length; item++) {
					cat.children[item].style.display = "block";
				}
			}
		}
	})();

	(function () {
		var iconSearch = document.querySelector(".js-header__search");
		var search = document.querySelector(".js-search");
		var closeSearch = document.querySelector(".js-close-search");
		var searchField = document.querySelector(".js-search-field");
		var searchResult = document.querySelector("#js-search-result");
		var searchMenu = document.querySelector(".js-search-menu");
		iconSearch.addEventListener("click", function () {
			search.classList.add("active");
		})

		closeSearch.addEventListener("click", function () {
			this.parentNode.classList.remove("active");
		})
		document.addEventListener("keydown", function (e) {
			if (e.keyCode == 27) {
				search.classList.remove("active");
			}
		})

		searchField.addEventListener("input", function () {
			searchResult.classList.add("active");
			searchMenu.classList.add("hide");
		})

		searchField.addEventListener("blur", function () {
			if (!this.value) {
				searchResult.classList.remove("active");
				searchMenu.classList.remove("hide");
			}
		})

		search.addEventListener("click", function () {
			searchResult.classList.remove("active");
			searchMenu.classList.remove("hide");
			searchField.value = "";
		})
	})();

	(function () {

		var tabsLinks = document.querySelectorAll(".js-tabs__link");
		var tabsContent = document.querySelectorAll(".js-tabs__item");

		tabsLinks.forEach(function (item) {

			if ($(item).parents(".js-about")) {

				if ($(item).attr("href") == "#js-tabs__item-1") {

					$(item).parents(".js-about").find(".about__content").addClass("padding");
				}

			}

			item.addEventListener("click", function (e) {
				if ($(item).parents(".js-about")) {
					if ($(item).attr("href") == "#js-tabs__item-1") {
						$(item).parents(".js-about").find(".about__content").addClass("padding");
					}
					else {
						$(item).parents(".js-about").find(".about__content").removeClass("padding");
					}
				}
				for (var i = 0; i < tabsContent.length; i++) {
					tabsContent[i].classList.remove("active");
				}
				for (var j = 0; j < tabsLinks.length; j++) {
					tabsLinks[j].classList.remove("active");
				}


				if ($(item).parents(".js-designers_partners")) {
					if ($(item).attr("href") == "#js-tabs__item-2") {
						$(item).parents(".js-designers_partners").addClass("other");
					}
					else {
						$(item).parents(".js-designers_partners").removeClass("other");
					}
				}

				item.classList.add("active");
				var href = item.getAttribute("href");
				var content = document.querySelector(href);
				content.classList.add("active");
				e.preventDefault();
			});
		});
	})();

	(function () {
		var tabBlock = document.querySelectorAll(".js-tab-block");
		Array.prototype.forEach.call(tabBlock, function(child) {
			var tabsLinks = child.querySelectorAll(".js-tab-block__link");
			var tabsContent = child.querySelectorAll(".js-tab-block__item");

			tabsLinks.forEach(function (item) {
				item.addEventListener("click", function (e) {
					for (var i = 0; i < tabsContent.length; i++) {
						tabsContent[i].classList.remove("active");
					}

					for (var j = 0; j < tabsLinks.length; j++) {
						tabsLinks[j].classList.remove("active");
					}

					item.classList.add("active");
					var href = item.getAttribute("href");
					var content = document.querySelector(href);
					content.classList.add("active");
					e.preventDefault();
				})
			})
		});

	})();

	(function () {
		var catalogItem = document.querySelectorAll(".js-catalog-product");

		Array.prototype.forEach.call(catalogItem, function (child) {
			var catalogOverlay = child.querySelectorAll(".js-catalog__item");
			var catalogHover = child.querySelectorAll(".js-catalog-hover");
			for (let i = 0; i < catalogOverlay.length; i++) {
				catalogOverlay[i].addEventListener("mouseover", function () {
					catalogHover[i].classList.add("active");
				})
				catalogOverlay[i].addEventListener("mouseout", function () {
					catalogHover[i].classList.remove("active");
					catalogHover[0].classList.add("active");
				})

				var manager = new Hammer(catalogOverlay[i]);
				var deltaX = 0;
				var deltaY = 0;
				manager.on('swipe', function (e) {
					deltaX = deltaX + e.deltaX;
					var direction = e.offsetDirection;
					var translate3d = 'translate3d(' + deltaX + 'px, 0, 0)';

					if (direction === 2 || direction === 4) {
						for (let i = 0; i < catalogHover.length; i++) {
							catalogHover[i].classList.remove("active");
							catalogOverlay[i].classList.remove("active");
						}
						catalogHover[i].classList.add("active");
						catalogOverlay[i].classList.add("active");
					}
				});
			}
		});
	})();

	(function () {
		var viewItem = document.querySelectorAll(".js-view__item");

		Array.prototype.forEach.call(viewItem, function (child) {
			var viewOverlay = child.querySelectorAll(".js-view__overlay-item");
			var viewBlock = child.querySelectorAll(".js-view__block");
			for (let i = 0; i < viewOverlay.length; i++) {
				viewOverlay[i].addEventListener("mouseover", function () {
					for (let j = 0; j < viewBlock.length; j++) {
						viewBlock[j].classList.remove("active");
					}
					viewBlock[i].classList.add("active");
				})
				viewOverlay[i].addEventListener("mouseout", function () {
					viewBlock[i].classList.remove("active");
				})

				var viewHummer = new Hammer(viewOverlay[i]);
				var deltaX = 0;
				var deltaY = 0;
				viewHummer.on('swipe', function (e) {
					deltaX = deltaX + e.deltaX;
					var direction = e.offsetDirection;
					var translate3d = 'translate3d(' + deltaX + 'px, 0, 0)';

					if (direction === 2 || direction === 4) {

						for (let i = 0; i < viewBlock.length; i++) {
							viewBlock[i].classList.remove("active");
							viewOverlay[i].classList.remove("active");
						}
						viewBlock[i].classList.add("active");
						viewOverlay[i].classList.add("active");
					}
				});
			}

			child.addEventListener("mouseout", function () {
				viewBlock[0].classList.add("active");
			})
		});

	})();


	(function selectTop() {
		var headerTop = document.querySelector(".js-header-top");

		function CustomSelect(options) {
			var elem = options.elem;

			elem.onclick = function (event) {
				if (event.target.parentNode.className == 'custom-select__button-city' || event.target.className == 'custom-select__button-city' || event.target.className == 'custom-select__button-address') {
					toggle();
				} else if (event.target.tagName == 'SPAN') {
					$(".custom-select__item").removeClass("active");
					event.target.parentNode.classList.add("active");
					var text = event.target.parentNode.querySelector(".custom-select__town").innerHTML;
					if (address) {
						var address = event.target.parentNode.querySelector(".custom-select__address").innerHTML;
					}
					setValue(text, address, event.target.parentNode.dataset.value);
					close();
				}
			}

			var isOpen = false;

			// ------ обработчики ------

			// закрыть селект, если клик вне его
			function onDocumentClick(event) {
				if (!elem.contains(event.target)) close();
			}

			// ------------------------

			function setValue(title, address, value) {
				elem.querySelector('.custom-select__button-city span').innerHTML = title;
				if (address) {
					elem.querySelector('.custom-select__button-address').innerHTML = address;
				}
				headerTop.classList = "header__top  js-header-top";
				headerTop.classList.add('header__top_' + value);
				var widgetEvent = new CustomEvent('select', {
					bubbles: true,
					detail: {
						title: title,
						value: value
					}
				});

				elem.dispatchEvent(widgetEvent);

			}

			function toggle() {
				if (isOpen) close()
				else open();
			}

			function open() {
				elem.classList.add('open');
				document.addEventListener('click', onDocumentClick);
				isOpen = true;
			}

			function close() {
				elem.classList.remove('open');
				document.removeEventListener('click', onDocumentClick);
				isOpen = false;
			}

		}

		var citySelect = new CustomSelect({
			elem: document.querySelector('.js-city-select')
		});
	})();

	(function selectTopBrand() {

		function CustomSelect(options) {
			var elem = options.elem;

			elem.onclick = function (event) {
				if (event.target.parentNode.className == 'custom-select__button-city' || event.target.className == 'custom-select__button-city' || event.target.className == 'custom-select__button-address') {
					toggle();
				} else if (event.target.tagName == 'SPAN') {
					$(".custom-select__item").removeClass("active");
					event.target.parentNode.classList.add("active");
					var text = event.target.parentNode.querySelector(".custom-select__town").innerHTML;
					if (address) {
						var address = event.target.parentNode.querySelector(".custom-select__address").innerHTML;
					}
					setValue(text, address, event.target.parentNode.dataset.value);
					close();
				}
			}

			var isOpen = false;

			// ------ обработчики ------

			// закрыть селект, если клик вне его
			function onDocumentClick(event) {
				if (!elem.contains(event.target)) close();
			}

			// ------------------------

			function setValue(title, address, value) {
				elem.querySelector('.custom-select__button-city span').innerHTML = title;
				if (address) {
					elem.querySelector('.custom-select__button-address').innerHTML = address;
				}
				headerTop.classList = "header__top  js-header-top";
				headerTop.classList.add('header__top_' + value);
				var widgetEvent = new CustomEvent('select', {
					bubbles: true,
					detail: {
						title: title,
						value: value
					}
				});

				elem.dispatchEvent(widgetEvent);

			}

			function toggle() {
				if (isOpen) close()
				else open();
			}

			function open() {
				elem.classList.add('open');
				document.addEventListener('click', onDocumentClick);
				isOpen = true;
			}

			function close() {
				elem.classList.remove('open');
				document.removeEventListener('click', onDocumentClick);
				isOpen = false;
			}

		}

		if (document.querySelector('.js-brand-select')) {
			var brandSelect = new CustomSelect({
				elem: document.querySelector('.js-brand-select')
			});
		}


	})();

	(function () {
		var cardcolor = document.querySelector(".js-card-color");
		var cardItems = document.querySelector(".js-card-items");

		if (!cardcolor) {
			return false
		}

		cardcolor.addEventListener("click", function () {
			this.classList.toggle("active");
			cardItems.classList.toggle("active");
		})

	})();

	(function () {
		var aboutMore = document.querySelector(".js-about__more");
		var aboutBlocks = document.querySelectorAll(".js-about__block");

		if (!aboutMore) {
			return false;
		}

		aboutMore.addEventListener("click", function () {

			Array.prototype.forEach.call(aboutBlocks, function (child) {
				child.classList.toggle("hide");
			});
		})
	})();

	(function () {
		var aboutText = document.querySelector(".js-about__explanation");
		var aboutMore = document.querySelector(".js-about__all");

		if (!aboutText) {
			return false;
		}

		aboutMore.addEventListener("click", function () {
			aboutText.classList.toggle("active");
		})
	})();


	(function showHidedSelections() {
		var selectionButton = document.querySelector(".js-selection__more");
		var selectionItems = document.querySelectorAll(".js-selection__hided");

		if (!selectionButton) {
			return false;
		}

		selectionButton.addEventListener("click", function () {
			Array.prototype.forEach.call(selectionItems, function (child) {
				child.classList.toggle("hide");
			});
			selectionButton.classList.toggle("active");
		})
	})();

	function validate(form) {
		var email = $(form).find(".js-required-email");
		var phone = $(form).find(".js-required-phone");
		var input = $(form).find('.js-required');
		var optional = $(form).find('.js-optional');
		var formSubmit = $(form).find('.js-form__submit');
		var formSubmitAlone = $('.js-form__submit');
		var completed;
		var email_regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,3})+$/;
		var result;

		var phoneMask;

		Array.prototype.forEach.call(phone, function (child) {
			phoneMask = new IMask(child, {
				mask: '000 000-00-00'
			});
		});

		function checkEmpty() {
			input.each(function () {
				if ($(this).val().trim() == "") {
					completed = false;
					return false;
				}
				return true;
			})
		}

		function checkEmail() {
			result = email_regex.test(email.val());
			if (!result) {
				completed = false;
				return false;
			}
			return true;
		}

		function checkPhone() {
			if (phoneMask.unmaskedValue.length !== 10) {
				completed = false;
				return false;
			}
			return true;
		}

		var tooltip = document.createElement("div");
		tooltip.classList.add("basket__tooltip");
		tooltip.innerHTML = "для оформления заказа необходимо заполнить обязательные поля";

		formSubmitAlone.on("click", function (e) {

			checkInput(input);

			if ($("#js-tabs__item-1").hasClass("active")) {
				if ($(".js-form").find(".js-required").hasClass("invalid")) {
					e.preventDefault();
					formSubmitAlone.after(tooltip);
				}

				else {

				}
			}

			else if ($("#js-tabs__item-2").hasClass("active")) {

				if ($(".js-form-second").find(".js-required").hasClass("invalid")) {
					e.preventDefault();

				}
				else {

				}
			}


		})

		function checkInputChange(element) {

			$.each(element, function () {
				$(this).on("change keyup input click", function () {
					$(".basket__tooltip").remove();
					if ($(this).attr("type") == "text") {
						if ($(this).val().trim() == "") {
							completed = false;
							$(this).removeClass("valid");
						} else {
							$(this).addClass("valid");
							completed = true;
						}
						if (phone.length) {
							checkPhone();
						}

						checkEmpty();

						if (email.length) {
							checkEmail();
						}

					}

					if ($(this).attr("type") == "email") {

						result = email_regex.test($(this).val());
						if (!result) {
							$(this).removeClass("valid");
							completed = false;
						} else {
							$(this).addClass("valid");
							completed = true;
						}

						checkEmpty();
						if (phone.length) {
							checkPhone();
						}


					}

					if ($(this).attr("type") == "tel") {
						if (phoneMask.unmaskedValue.length == 10) {
							$(this).addClass("valid");
							completed = true;
						} else {
							$(this).removeClass("valid");
							completed = false;
						}
						checkEmpty();
						if (email.length) {
							checkEmail();
						}
					}
					if (completed) {
						formSubmit.addClass("active");
					}
					else {
						formSubmit.removeClass("active");
					}

					if (completed) {
						formSubmitAlone.addClass("active");
						formSubmit.attr("disabled",false);
						$(".basket__tooltip").remove();
					}
					else {
						formSubmitAlone.removeClass("active");
						formSubmit.attr("disabled",true);
					}
				})
			})
		}

		checkInputChange(input);

		function checkInputOptional(element) {

			$.each(element, function () {

				$(this).on("change keyup input click", function () {
					if (!$(this).val().trim() == "") {
						$(this).addClass("valid");
					} else {
						$(this).removeClass("valid");
					}

				})
			})
		}

		checkInputOptional(optional);

		function addError(element) {
			element.addClass("invalid").removeClass("valid");
			displayError(element, true);
		}

		function removeError(element) {
			element.removeClass("invalid").addClass("valid");
			displayError(element, false);
		}

		function checkInput(element) {
			$.each(element, function () {

				if ($(this).val().trim() == "") {
					addError($(this));
				}
				else if (($(this).val().trim() !== "") && ($(this).attr("type") == "text")) {
					removeError($(this));
				}

				if ($(this).attr("type") == "email") {
					if (!checkEmail()) {
						addError($(this));
					}
					else {
						removeError($(this));
					}
				}

				if ($(this).attr("type") == "tel") {
					if (!checkPhone()) {
						addError($(this));
						$(this).parent().addClass("invalid");
					}
					else {
						removeError($(this));
						$(this).parent().removeClass("invalid");
					}
				}

			})
		}

		function checkInputBlur(element) {
			$.each(element, function () {
				$(this).on("blur", function () {

					if ($(this).val().trim() == "") {
						addError($(this));
					}
					else if (($(this).val().trim() !== "") && ($(this).attr("type") == "text")) {
						removeError($(this));
					}

					if ($(this).attr("type") == "email") {
						if (!checkEmail()) {
							addError($(this));
						}
						else {
							removeError($(this));
						}
					}

					if ($(this).attr("type") == "tel") {
						if (!checkPhone()) {
							addError($(this));
							$(this).parent().addClass("invalid");
						}
						else {
							removeError($(this));
							$(this).parent().removeClass("invalid");
						}
					}
				})
			})
		}

		function displayError(element, invalid) {

			function showError() {

				if (!element.parent().find(".form-inputs__error").length) {

					var message = document.createElement("div");
					message.classList.add("form-inputs__error");
					if (element.val().trim() == "") {
						message.innerHTML = "Заполните поле"
					}
					else if ((element.attr("type") !== "text") && (element.val().trim() !== "")) {
						message.innerHTML = element.attr("title");
					}

					element.parent().append(message);
				}

				else if (element.parent().find(".form-inputs__error").length) {
					if ((element.attr("type") == "text") && (element.val().trim() == "")) {
						element.parent().find(".form-inputs__error").html("Заполните поле");
					}
					else if ((element.attr("type") !== "text") && (element.val().trim() !== "")) {
						element.parent().find(".form-inputs__error").html(element.attr("title"));
					}
					else if ((element.attr("type") !== "text") && (element.val().trim() == "")) {
						element.parent().find(".form-inputs__error").html(element.attr("data-empty"));
					}
				}
			}

			if (!element.hasClass("valid")) {
				showError();
				element.parent().find(".form-inputs__error").removeClass("active").removeClass("inactive");
			}
			else {
				element.parent().find(".form-inputs__error").addClass("active").addClass("inactive");
			}
		}

		checkInputBlur(input);
	}

	validate(".js-form");
	validate(".js-form-second");

	(function () {
		var basketItem = document.querySelectorAll(".js-basket__item");

		if (!basketItem.length) {
			return false;
		}

		Array.prototype.forEach.call(basketItem, function (child) {
			var basketCount = child.querySelector(".js-count-field");
			var basketPlus = child.querySelector(".js-basket-plus");
			var basketMinus = child.querySelector(".js-basket-minus");
			var basketScore = child.querySelector(".js-basket__score");
			var basketPrice = child.querySelector(".js-basket__price");
			var basketNumber = child.querySelector(".js-basket__number");
			var basketField = child.querySelector(".js-count-field");
			var basketTrimPrice;
			var re = /(?=\B(?:\d{3})+(?!\d))/g;

			function changeValue() {
				if (basketField.value > 1) {
					basketPrice.classList.add("active");
					basketNumber.classList.add("active");
					basketTrimPrice = basketPrice.innerHTML.replace(/\D+/g, "");
					basketScore.innerHTML = basketField.value * basketTrimPrice + ' руб.';
					basketScore.innerHTML = basketScore.innerHTML.replace(re, ' ');
					basketNumber.innerHTML = basketField.value + " x "
				}
				else {
					basketPrice.classList.remove("active");
					basketNumber.classList.remove("active");
					basketScore.innerHTML = basketField.value * basketTrimPrice + ' руб.';
					basketScore.innerHTML = basketScore.innerHTML.replace(re, ' ');
				}
			}

			basketPlus.addEventListener("click", function () {
				basketCount.value = Number(basketCount.value) + 1;
				changeValue();
			})

			basketMinus.addEventListener("click", function () {
				if (basketCount.value <= 1) {
					basketCount.value = 1;
				} else {
					basketCount.value = Number(basketCount.value) - 1;
				}
				changeValue();
			})
		});
	})();

	(function basketItem() {
		var basketItem = document.querySelectorAll(".js-basket__item");

		if (!basketItem.length) {
			return false;
		}

		Array.prototype.forEach.call(basketItem, function (child) {
			var basketReturn = child.querySelector(".js-basket-return");
			var basketRemove = child.querySelector(".js-basket-remove");
			var basketActive = child.querySelectorAll(".js-basket-active");
			var basketHide = child.querySelectorAll(".js-basket-hide");
			var basketClose = child.querySelector(".js-basket-close");
			var basketDesc = child.querySelector(".js-basket-desc");

			basketClose.addEventListener("click", function () {
				for (let i = 0; i < basketActive.length; i++) {
					basketActive[i].classList.add("hide");
				}
				for (let j = 0; j < basketHide.length; j++) {
					basketHide[j].classList.add("active");
				}
				basketDesc.classList.add("hide");
			})

			basketReturn.addEventListener("click", function () {
				for (let i = 0; i < basketActive.length; i++) {
					basketActive[i].classList.remove("hide");

				}
				for (let j = 0; j < basketHide.length; j++) {
					basketHide[j].classList.remove("active");
				}
				basketDesc.classList.remove("hide");
			})

			basketRemove.addEventListener("click", function () {
				var basketParent = basketRemove.parentNode.parentNode.parentNode.parentNode;
				basketParent.parentNode.removeChild(basketParent);
			})
		})
	})();

	(function accordion() {
		var acc = document.getElementsByClassName("js-accordion__button");
		var i;

		if (!acc.length) {
			return false;
		}

		for (i = 0; i < acc.length; i++) {
			acc[i].addEventListener("click", function () {
				this.classList.toggle("active");

				var panel = this.nextElementSibling;
				panel.classList.toggle("active");
			});
		}
	})();

	(function searchMoreButton() {
		var searchButton = document.querySelector(".js-search-list__more");
		var searchBlock = document.querySelector(".js-search-list__block");
		var searchitem = document.querySelectorAll(".search-list__link");
		var searchHidden = document.querySelector(".js-search-list__hidden");


		if (!searchButton) {
			return false;
		}

		searchButton.addEventListener("click", function () {
			overlay.classList.add("flag");
			searchButton.classList.toggle("active");
			searchBlock.classList.toggle("active");
			searchHidden.classList.toggle("active");
		})

		for (let i = 0; i < searchitem.length; i++) {
			searchitem[i].addEventListener("click", function () {
				searchButton.classList.remove("active");
				searchBlock.classList.remove("active");
				searchHidden.classList.remove("active");
				overlay.classList.remove("flag");
			})
		}


		overlay.addEventListener("click", function () {
			searchButton.classList.remove("active");
			searchBlock.classList.remove("active");
			searchHidden.classList.remove("active");
			overlay.classList.remove("flag");
		})
	})();
	var scroolValue;

	(function filter() {
		var filter = document.querySelector(".js-filter");
		var filterItem = document.querySelectorAll(".js-filter__item");
		var filterMobile = document.querySelector(".js-filter__mobile");
		var filterChange = document.querySelector(".js-filter__change");
		var filterButtonReset = document.querySelector(".js-filter-button-reset");
		var filterButtonApply = $(".js-filter__mobile-apply");

		if (!filterItem.length) {
			return false;
		}


		if (mq.matches) {
			filterButtonApply.on("click", function () {
				$(".filter__item").removeClass("visible");
				$(".filter__item").removeClass("active");
				filter.classList.remove("active");
				filterMobile.classList.remove("active");
				$(".wrapper").scrollTop(scroolValue);
			})
		}

		filterButtonReset.addEventListener("click", function () {

			for (let i = 0; i < filterItem.length; i++) {
				var otherChecks = filterItem[i].querySelectorAll(".js-filter-check");
				var filterCount = filterItem[i].querySelector(".js-filter__count");
				var filterSubmit = filterItem[i].querySelector(".js-filter__submit");
				var filterReset = filterItem[i].querySelector(".js-filter__reset");

				if (mq.matches) {

				} else {
					filterItem[i].classList.remove("active");
				}

				filterItem[i].classList.remove("checked");
				if (filterCount) {
					filterCount.classList.remove("active");
					filterCount.innerHTML = "";
				}

				if (filterSubmit) {
					filterSubmit.disabled = true;
					filterReset.disabled = true;
				}

				for (let k = 0; k < otherChecks.length; k++) {
					if (otherChecks[k].classList.contains("active")) {
						otherChecks[k].classList.remove("active");
					}
					else {
						otherChecks[k].value = "";
					}

				}

			}
			filterButtonReset.classList.remove("active");
		})

		Array.prototype.forEach.call(filterItem, function (child) {
			var filterText = child.querySelector(".js-filter__text");
			var filterInputs = child.querySelectorAll(".js-filter__input");
			var filterSubmit = child.querySelector(".js-filter__submit");
			var filterReset = child.querySelector(".js-filter__reset");
			var filterButtons = child.querySelectorAll(".js-filter__button");
			var filterColors = child.querySelectorAll(".js-filter__color");
			var filterCount = child.querySelector(".js-filter__count");
			var filterView = child.querySelectorAll(".js-filter__view");
			var filterWeight = child.querySelectorAll(".js-filter__weight");
			var filterCheck = child.querySelectorAll(".js-filter-check");
			var active = false;
			var mq = window.matchMedia("(min-width: 768px)");

			if (filterReset) {
				filterReset.addEventListener("click", function () {
					for (let j = 0; j < filterCheck.length; j++) {
						filterCheck[j].classList.remove("active");
						filterSubmit.disabled = true;
						filterReset.disabled = true;
						if (filterCheck[j].classList.contains("js-filter__input")) {
							filterCheck[j].value = "";
						}
						if (filterCount) {
							filterCount.innerHTML = ""
						}

						checkFilters();
					}
				})
			}


			filterText.addEventListener("click", function () {

				if (mq.matches) {
					overlay.classList.add("flag");
				}

				if (mq.matches) {
					for (let i = 0; i < filterItem.length; i++) {
						if (filterItem[i].classList.contains("active") && filterItem[i] !== child) {
							filterItem[i].classList.remove("active");
						}
					}
				}


				filterText.parentNode.classList.toggle("active");

			})

			overlay.addEventListener("click", function () {
				for (let i = 0; i < filterItem.length; i++) {
					filterItem[i].classList.remove("active");
				}
			})


			for (let i = 0; i < filterInputs.length; i++) {

				filterInputs[i].addEventListener("input", function () {
					if (filterInputs[i].value) {
						filterDisable = false;
						filterButtonReset.classList.add("active");
						checkFilters();
					}
					filterSubmit.disabled = filterDisable;
					filterReset.disabled = filterDisable;
				})
			}

			for (let i = 0; i < filterButtons.length; i++) {

				filterButtons[i].addEventListener("click", function () {

					for (let j = 0; j < filterButtons.length; j++) {
						filterButtons[j].classList.remove("active");
					}

					filterText.innerHTML = this.innerHTML;
					filterButtons[i].classList.add("active");
					checkFilters();
				})
			}

			function checkFilters() {
				var mount = false;
				var filterOtherColors = filter.querySelectorAll(".js-filter__color");
				var filterOtherWeight = filter.querySelectorAll(".js-filter__weight");
				var filterOtherView = filter.querySelectorAll(".js-filter__view");
				var filterOtherInput = filter.querySelectorAll(".js-filter__input");

				for (let i = 0; i < filterCheck.length; i++) {
					if (filterCheck[i].classList.contains("active") || filterCheck[i].value) {
						mount = true;
						$(filterCheck[i]).parents(".js-filter__item").addClass("checked");
					}
				}
				for (let i = 0; i < filterItem.length; i++) {
					var otherChecks = filterItem[i].querySelectorAll(".js-filter-check");
					for (let k = 0; k < otherChecks.length; k++) {
						if (otherChecks[k].classList.contains("active") || otherChecks[k].value) {
							mount = true;
						}
					}

				}

				if (mount) {
					filterButtonReset.classList.add("active");
				} else {
					filterButtonReset.classList.remove("active");
				}
			}

			for (let i = 0; i < filterColors.length; i++) {

				filterColors[i].addEventListener("click", function () {
					filterColors[i].classList.toggle("active");

					var count = 0;

					for (let i = 0; i < filterColors.length; i++) {

						if (filterColors[i].classList.contains("active")) {
							count++;
						}
					}

					if (count) {
						filterCount.innerHTML = count;
						filterCount.classList.add("active");
						filterSubmit.disabled = false;
						filterReset.disabled = false;
						child.classList.add("checked");
						checkFilters();

					} else {
						filterCount.innerHTML = "";
						filterCount.classList.remove("active");
						filterSubmit.disabled = true;
						filterReset.disabled = true;
						child.classList.remove("checked");
						checkFilters();

					}
				})
			}

			for (let i = 0; i < filterView.length; i++) {

				filterView[i].addEventListener("click", function () {

					for (let j = 0; j < filterView.length; j++) {

						if (filterView[j].classList.contains("active") && filterView[i] !== child) {
							filterView[j].classList.remove("active");
						}
						filterView[j].classList.remove("active");
					}

					filterView[i].classList.toggle("active");

					var count = false;

					for (let j = 0; j < filterView.length; j++) {
						if (filterView[j].classList.contains("active")) {
							count = true;
							checkFilters();
						}
					}

					if (count) {
						filterSubmit.disabled = false;
						filterReset.disabled = false;
						checkFilters();
					} else {
						filterSubmit.disabled = true;
						filterReset.disabled = true;
						checkFilters();
					}
				})
			}

			for (let i = 0; i < filterWeight.length; i++) {

				filterWeight[i].addEventListener("click", function () {

					filterWeight[i].classList.toggle("active");

					var count = false;

					for (let j = 0; j < filterWeight.length; j++) {
						if (filterWeight[j].classList.contains("active")) {
							count = true;
						}
					}

					for (let j = 0; j < filterView.length; j++) {
						if (filterView[j].classList.contains("active")) {
							count = true;
						}
					}

					if (count) {
						filterSubmit.disabled = false;
						filterReset.disabled = false;
						filterWeight[i].classList.add("checked");
						checkFilters();
					} else {
						filterSubmit.disabled = true;
						filterReset.disabled = true;
						filterWeight[i].classList.remove("checked");
						checkFilters();
					}
				})
			}


		});
	})();
	(function () {
		var filter = document.querySelector(".js-filter");
		var filterTablet = document.querySelector(".js-filter__tablet");
		var filterDesktop = document.querySelector(".js-filter__desktop");
		var filterMobile = document.querySelector(".js-filter__mobile");
		var filterChange = document.querySelector(".js-filter__change");
		var filteritem = document.querySelectorAll(".js-filter .filter__item");

		if (!filterChange) {
			return false;
		}

		var mq = window.matchMedia("(min-width: 768px)");

		if (mq.matches) {
			filterChange.addEventListener("click", function () {
				filterTablet.classList.toggle("active");
				filterChange.classList.toggle("active");
			})
		} else {
			filterChange.addEventListener("click", function () {
				filter.classList.remove("hide");
				$(".filter__item").addClass("visible");
				filterMobile.classList.toggle("active");
				filterMobile.parentNode.classList.add("active");
				scroolValue = $(".wrapper").scrollTop();
				$(".wrapper").scrollTop(0);
			})
		}
	})();
	(function () {
		var inputs = document.querySelectorAll('.js-file');

		if (!inputs.length) {
			return false;
		}

		Array.prototype.forEach.call(inputs, function (input) {
			var label = input.nextElementSibling,
				results = label.nextElementSibling;


			input.addEventListener('change', function (e) {
				var fileName = '';
				for (let i = 0; i < this.files.length; i++) {
					var size = this.files[i].size;
					var sizeInMB = (size / (1024 * 1024)).toFixed(2);
					var item = document.createElement("li");
					item.classList.add("file__item");
					item.innerHTML = this.files[i].name;
					var itemSize = document.createElement("span");
					itemSize.classList.add("file__size");
					itemSize.innerHTML = sizeInMB + ' мб.'
					var close = document.createElement("button");
					close.setAttribute("type", "button");
					close.classList.add("file__delete", "js-file-delete");
					item.appendChild(itemSize);
					item.appendChild(close);
					results.appendChild(item);
				}
			});

			document.addEventListener("click", function (e) {
				if (e.target.classList.contains("js-file-delete")) {
					e.target.parentNode.parentNode.removeChild(e.target.parentNode);
				}
			})
		});
	})();

	(function () {
		var items = document.querySelectorAll(".js-dots");

		if (!items.length) {
			return false;
		}

		for (var i = 0; i < items.length; i++) {
			items[i].innerHTML = items[i].innerHTML + '......................................................................................................................';
		}
	})();

	(function () {
		$(".wrapper").on("scroll", function () {
			$(".tooltip").remove();
			$(".jqvmap-label").css("display", "none");
		})
	})();

	(function () {
		$(".wrapper").on("scroll", function () {
			if (!$(".js-favorite").length) {
				return false;
			}
			if (($(".wrapper").scrollTop() > 270) && ($(".js-recent").offset().top > 150)) {
				$('.js-favorite-fixed').addClass('fixed');
				$(".js-favorite").addClass("padding");
			}
			else {
				$('.js-favorite-fixed').removeClass('fixed');
				$(".js-favorite").removeClass("padding");
			}
		});
	})();

	(function () {
		$(".wrapper").on("scroll", function () {
			if ($(".wrapper").scrollTop() > 310) {
				$(".js-search-page").addClass("padding");
				$('.js-search-page-fixed').addClass('fixed');

			}
			else {
				$(".js-search-page").removeClass("padding");
				$('.js-search-page-fixed').removeClass('fixed');

			}
		});
	})();

	(function () {
		$(".wrapper").on("scroll", function () {
			if ($(".wrapper").scrollTop() > 270) {
				$('.js-basket-fixed').addClass('fixed');
				$(".js-basket-padding").addClass("padding");
			}
			else {
				$('.js-basket-fixed').removeClass('fixed');
				$(".js-basket-padding").removeClass("padding");
			}
		});
	})();

	(function () {
		if (!$(".card_premium").length) {
			return false;
		}
		var tempScrollTop, currentScrollTop = 0;

		$(".wrapper").scroll(function () {

			currentScrollTop = $(".wrapper").scrollTop();

			if (tempScrollTop < currentScrollTop) {//scrolling down
				if (($(".js-premium").offset().top < 0) && ($(".js-about").offset().top > 0)) {
					$(".js-card-fixed").addClass("fixed");
				} else {
					$(".js-card-fixed").removeClass("fixed");
				}
			}

			else if (tempScrollTop > currentScrollTop) {//scrolling up
				$(".js-card-fixed").addClass("fixed");
				if ((-$(".js-card").offset().top) < $(".js-card").outerHeight()) {
					$(".js-card-fixed").removeClass("fixed");
				}
			}

			tempScrollTop = currentScrollTop;
		})
	})();

	(function () {
		var searchField = document.querySelector(".js-search-page__field");
		var searchLabel = document.querySelector(".js-search-page__label");
		if (!searchField) {
			return false;
		}

		searchField.addEventListener("change", function () {
			if (searchField.value) {
				searchLabel.classList.add("inactive");
			}
		})


	})();

	(function () {
		$(".js-compare-icon").on("click", function () {
			$(".js-compare-menu").addClass("active");
			overlay.classList.add("flag");
		})

		overlay.addEventListener("click", function (e) {
			$(".js-compare-menu").removeClass("active");
		})
	})();

	(function () {
		var mq = window.matchMedia("(min-width: 768px)");
		if (mq.matches) {
			var serviceSlice = document.querySelectorAll(".slice__item");
			if (!serviceSlice.length) {
				return false;
			}
			for (let i = 0; i < serviceSlice.length; i++) {
				serviceSlice[i].addEventListener("mouseover", function () {
					for (let j = 0; j < serviceSlice.length; j++) {
						serviceSlice[j].classList.add("inactive")
					}
					serviceSlice[i].classList.remove("inactive");
					serviceSlice[i].classList.add("active");
				})
				serviceSlice[i].addEventListener("mouseout", function () {

					for (let j = 0; j < serviceSlice.length; j++) {
						serviceSlice[j].classList.remove("inactive")
					}
					serviceSlice[i].classList.remove("active");
				})
			}
		}

	})();

	(function () {
		var serviceSelect = document.querySelector(".js-service-select");
		var mq = window.matchMedia("(min-width: 768px)");

		if (!serviceSelect) {
			return false;
		}
		var serviceList = document.querySelector(".js-service-list");
		var tabsLinks = serviceList.querySelectorAll(".js-tabs__link");
		var text;

		for (let i = 0; i < tabsLinks.length; i++) {
			if (tabsLinks[i].classList.contains("active")) {
				serviceSelect.innerHTML = tabsLinks[i].innerHTML;
			}
			tabsLinks[i].addEventListener("click", function () {
				serviceSelect.classList.remove("active");
				serviceList.classList.remove("active");
				overlay.classList.remove("flag")
				serviceSelect.innerHTML = tabsLinks[i].innerHTML;

				$('.js-delivery-row__item').removeClass("active");
				if (mq.matches) {
					if (tabsLinks[i].innerHTML == "Подключение") {
						if (!$("#vmap2").hasClass("active")) {
							readymap2();
						}
					}

					if (tabsLinks[i].innerHTML == "Доставка") {
						if (!$("#vmap").hasClass("active")) {
							readymap1();
						}

					}
				}


			})
		}

		serviceSelect.addEventListener("click", function () {
			serviceSelect.classList.toggle("active");
			serviceList.classList.toggle("active");
			overlay.classList.toggle("flag")
		})

		overlay.addEventListener("click", function () {
			serviceSelect.classList.remove("active");
			serviceList.classList.remove("active");
		})


	})();

	(function () {
		var centersMore = document.querySelectorAll(".js-centers-more");

		if (!centersMore.length) {
			return false;
		}

		for (let i = 0; i < centersMore.length; i++) {
			centersMore[i].addEventListener("click", function () {
				centersMore[i].nextElementSibling.classList.toggle("active");
			})
		}
	})();

	(function () {
		var collectionButton = document.querySelector(".js-collection__button");
		var collectionHidden = document.querySelector(".js-collection__hidden");
		var collectionReturn = document.querySelector(".js-collection__return");

		if (!collectionButton) {
			return false;
		}

		collectionButton.addEventListener("click", function () {
			collectionHidden.classList.toggle("active");
			collectionButton.classList.toggle("active");
			overlay.classList.toggle("active");

			if (mq.matches) {
				if (collectionHidden.classList.contains("active")) {
					overlay.style.display = "block";
				}
				else {
					overlay.style.display = "none";
				}

			}
		})

		collectionReturn.addEventListener("click", function () {
			collectionHidden.classList.remove("active");
			collectionButton.classList.remove("active");
			overlay.classList.remove("active");
			if (mq.matches) {
				overlay.style.display = "none";
			}
		})

		overlay.addEventListener("click", function () {
			collectionHidden.classList.remove("active");
			collectionButton.classList.remove("active");
			if (mq.matches) {
				overlay.style.display = "none";
			}
		})

	})();

	(function () {
		var companionSelect = $(".js-companion__select");
		var companionHidden = $(".js-companion__hidden");

		companionSelect.on("click", function () {
			companionHidden.toggleClass("active");
			collectionSwiper.update();
		})
	})();

	(function () {
		var reviewsMore = $(".js-reviews__view");

		reviewsMore.on("click", function () {
			$(this).parent().css("height", "auto");
			$(this).remove();
		})
	})();

});