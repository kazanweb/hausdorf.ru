var mq = window.matchMedia("(min-width: 768px)");
if (mq.matches) {
	function readymap1() {
		var items1 = $('.delivery-row_shipping .js-delivery-row__item');
		var more = $('.delivery-row_shipping .js-delivery-row__more');
		var data_obj = {};
		var selectedId = '';
		var items1Hidden = [];
		$('#vmap').addClass("active");
		$(items1).each(function (i) {

			var data = this.getAttribute('data-region');
			data_obj[data] = '';

			if ($(this).hasClass('active')) {
				selectedId = '#jqvmap1_' + data;
			}

			$(this).click(function () {
				$(items1).removeClass('active');
				selectedId = '#jqvmap1_' + data;
				selectedId2 = '#jqvmap2_' + data;

				$('#vmap').find(selectedId).click();
				$('#vmap').find(selectedId2).click();
			});

			if (i > 5) {
				$(more).show();
				this.style.display = "none";
				items1Hidden.push(this);
			}

		});

		$(more).click(function () {

			if ($(this).hasClass('active')) {
				$(items1Hidden).slideUp();
				$(this).removeClass('active');
				return false;
			}
			$(this).addClass('active');
			$(items1Hidden).slideDown();
		});

		var selectRegion = '#f0c3c7'; // Цвет изначально подсвеченных регионов
		var highlighted_states = {};
		//  Массив подсвечиваемых регионов, указанных в массиве data_obj
		for (iso in data_obj) {
			highlighted_states[iso] = selectRegion;
		}


		$('#vmap').vectorMap({
			map: 'russia_en',
			borderColor: '#ffffff',
			borderWidth: 2,
			backgroundColor: '#ffffff',
			color: '#efefef',
			colors: highlighted_states,
			hoverColor: '#ed939b',
			hoverOpacity: null,
			selectedColor: '#de000d',
			enableZoom: false,
			showTooltip: true,
			scaleColors: ['#C8EEFF', '#006491'],
			normalizeFunction: 'polynomial',
			// Клик по региону
			onRegionClick: function (element, code, region) {

				$(items1).removeClass('active');
				$('[data-region=' + code + ']').addClass('active');

			}
		});
		$(selectedId).click();
	}

	function readymap2() {
		var items1 = $('.delivery-row_connect .js-delivery-row__item');
		var more = $('.delivery-row_connect .js-delivery-row__more');
		var data_obj = {};
		var selectedId = '';
		var items1Hidden = [];
		$('#vmap2').addClass("active");
		$(items1).each(function (i) {

			var data = this.getAttribute('data-region');
			data_obj[data] = '';

			if ($(this).hasClass('active')) {
				selectedId = '#jqvmap1_' + data;
			}

			$(this).click(function () {
				$(items1).removeClass('active');
				selectedId = '#jqvmap1_' + data;
				selectedId2 = '#jqvmap2_' + data;
				$('#vmap2').find(selectedId).click();
				$('#vmap2').find(selectedId2).click();
			});

			if (i > 5) {
				$(more).show();
				this.style.display = "none";
				items1Hidden.push(this);
			}

		});

		$(more).click(function () {

			if ($(this).hasClass('active')) {
				$(items1Hidden).slideUp();
				$(this).removeClass('active');
				return false;
			}
			$(this).addClass('active');
			$(items1Hidden).slideDown();
		});

		var selectRegion = '#f0c3c7'; // Цвет изначально подсвеченных регионов
		var highlighted_states = {};
		//  Массив подсвечиваемых регионов, указанных в массиве data_obj
		for (iso in data_obj) {
			highlighted_states[iso] = selectRegion;
		}

		$('#vmap2').vectorMap({
			map: 'russia_en',
			borderColor: '#ffffff',
			borderWidth: 2,
			backgroundColor: '#ffffff',
			color: '#efefef',
			colors: highlighted_states,
			hoverColor: '#ed939b',
			hoverOpacity: null,
			selectedColor: '#de000d',
			enableZoom: false,
			showTooltip: true,
			scaleColors: ['#C8EEFF', '#006491'],
			normalizeFunction: 'polynomial',
			// Клик по региону
			onRegionClick: function (element, code, region) {

				$(items1).removeClass('active');
				$('[data-region=' + code + ']').addClass('active');

			}
		});
		$(selectedId).click();
	}
}


