var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
var player;

function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
		height: '100%',
		width: '100%',
		videoId: '82eRniMN6xQ',
		playerVars: {rel: 0},
		events: {}
	});
}

var smallVideo = document.querySelectorAll(".js-small-video");
for (let i = 0; i < smallVideo.length; i++) {
	var duration;
	var allTime;
	var currentTime;
	smallVideo[i].addEventListener("click", function () {
		for(let j=0;j<smallVideo.length;j++){
		  smallVideo[j].classList.remove("active");
		}
		allTime =  smallVideo[i].querySelector(".js-allTime");
		currentTime =  smallVideo[i].querySelector(".js-currentTime");
		smallVideo[i].classList.add("active");
		duration = smallVideo[i].querySelector(".js-about__duration");
		var dataVideo = smallVideo[i].getAttribute("data-video");
		player.videoId = dataVideo;
		player.playVideo();
		player.loadVideoById(dataVideo);
		setTimeout(setDurationVideo,3000);
		setInterval(getCurrentPosition,1000);
		setInterval(getCurrentTime,1000);

	})

	function getCurrentPosition() {
		var videoAllDuration = player.getDuration();
		var videoCurrentDuration = player.getCurrentTime();
		var percent = (videoCurrentDuration / videoAllDuration) * 100;

		duration.style.width = percent.toFixed() + "%";
	}

	function setDurationVideo() {
		var videoDuration = player.getDuration();

		function formatTime(time){
			time = Math.round(time);

			var minutes = Math.floor(time / 60),
				seconds = time - minutes * 60;

			seconds = seconds < 10 ? '0' + seconds : seconds;

			return minutes + ":" + seconds;
		}

		formatTime(videoDuration);
		allTime.innerHTML = formatTime(videoDuration);
	}

	function getCurrentTime() {
		var videoDuration = player.getCurrentTime();

		function formatTime(time){
			time = Math.round(time);

			var minutes = Math.floor(time / 60),
				seconds = time - minutes * 60;

			seconds = seconds < 10 ? '0' + seconds : seconds;

			return minutes + ":" + seconds;
		}

		formatTime(videoDuration);
		currentTime.innerHTML = formatTime(videoDuration);
	}
}


